// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  loginUrl: 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyAR4Yezxk7Ao4qeFntu7tIvE7pH28Eh64Y',
  fetchClientDetailsUrl: 'https://momentum-retail-practical-test.firebaseio.com/clients/${client.id}.json?auth=${client.token}',
  addAccountOnClientUrl: 'https://momentum-retail-practical-test.firebaseio.com/clients/${client.id}/accounts.json?auth=${client.token}',
  fetchAccountDetailsUrl: 'https://momentum-retail-practical-test.firebaseio.com/accounts/${accountNumber}.json?auth=${token}',
  updateAccountUrl: 'https://momentum-retail-practical-test.firebaseio.com/accounts/${account.accNumber}.json?auth=${token}',
  addAccountUrl: 'https://momentum-retail-practical-test.firebaseio.com/accounts/${account.accNumber}.json?auth=${token}'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
