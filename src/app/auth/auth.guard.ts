import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CacheService } from '../shared/cache.service';
import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'}) // Since this is a service, we need the @Injectable()
export class AuthGuard implements CanActivate {

  constructor(private cacheService: CacheService,
              private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, router: RouterStateSnapshot): boolean |
              UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    console.log("this.cacheService.get('userData')", this.cacheService.get('userData'));
    if (this.cacheService.get('userData')) {
      // If user logged in and navigating to login, it will redirect to the client route
      if (this.router.getCurrentNavigation().finalUrl.toString() === '/login') {
        this.router.navigate(['/client']);
        return false;
      }
      return true;
    } else {
      if (this.router.getCurrentNavigation().finalUrl.toString() === '/login') {
        return true;
      }
      this.router.navigate(['/login']);
      return false;
    }
  }

}
