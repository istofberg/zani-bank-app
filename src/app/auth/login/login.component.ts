import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { CacheService } from 'src/app/shared/cache.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  errorMessage: string;
  router: Router;

  constructor(private loginService: AuthService,
              private activatedRouter: Router,
              private cacheService: CacheService) {
    this.loginForm = new FormGroup({
      userData: new FormGroup({
        email: new FormControl(null, [Validators.required, Validators.email]),
        password: new FormControl(null, [Validators.required])
      })
    });
  }

  ngOnInit() {}

  onSubmit() {
    const formGroup = this.loginForm.controls.userData.value;
    this.loginService.loginUser(formGroup.email, formGroup.password).subscribe(user => {
      this.cacheService.set('userData', user);
      this.activatedRouter.navigate(['/client']);
    }, errorMessage => {
        this.errorMessage = this.mapError(errorMessage);
        console.log(errorMessage);
    });
  }

  mapError(errorMessage: string) {
    switch (errorMessage) {
      case 'EMAIL_NOT_FOUND': return 'Invalid email';
      case 'INVALID_PASSWORD': return 'Invalid password';
    }
  }

  resetForm() {
    this.loginForm.reset();
    this.errorMessage = null;
  }

}
