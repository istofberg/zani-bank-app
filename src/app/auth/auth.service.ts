import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { CacheService } from '../shared/cache.service';
import { Router } from '@angular/router';
import { Client } from '../client/client';
import { LoginResponseData } from './login/login-response-data';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient,
              private cacheService: CacheService,
              private router: Router) {}

  loginUser(email, password): Observable<Client> {
    return this.http
      .post<LoginResponseData>(environment.loginUrl, {
        email,
        password,
        returnSecureToken: true
      })
      .pipe(
        catchError(errorResponse => {
          return throwError(errorResponse.error.error.message);
        }),
        map(responseData => {
          return new Client(
            responseData.email,
            responseData.localId,
            responseData.expiresIn,
            responseData.idToken
          );
        }));
  }

  logoutUser() {
    if (this.cacheService.get('userData').token) {
      this.cacheService.clear();
      this.router.navigate(['/login']);
    }
  }

}
