import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ClientComponent } from './client.component';
import { ClientService } from './client.service';
import { AccountModule } from '../account/account.module';

@NgModule({
  declarations: [
    ClientComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AccountModule
  ],
  providers: [
    ClientService
  ]
})
export class ClientModule { }
