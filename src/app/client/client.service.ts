import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ClientDetails } from './client-details';
import { Client } from './client';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }

  fetchClientDetails(client: Client) {
    const url = environment.fetchClientDetailsUrl.replace('${client.id}', client.id)
                                                 .replace('${client.token}', client.token);
    return this.http.get<ClientDetails>(url);
  }

  addAccount(client: Client, accountNumbers: number[]) {
    const url = environment.addAccountOnClientUrl.replace('${client.id}', client.id)
                                                 .replace('${client.token}', client.token);
    return this.http.put(url, accountNumbers);
  }

}
