
export class ClientDetails {
  public accounts: number[];
  public age: number;
  public name: string;

  constructor(accounts: number[], age: number, name: string) {
    this.accounts = accounts;
    this.age = age;
    this.name = name;
  }

}
