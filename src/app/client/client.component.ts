import { Component, OnInit } from '@angular/core';
import { CacheService } from '../shared/cache.service';
import { Router } from '@angular/router';
import { ClientDetails } from './client-details';
import { ClientService } from './client.service';
import { Client } from './client';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {
  client: Client;
  clientDetails: ClientDetails;

  constructor(private clientService: ClientService,
              private cacheService: CacheService,
              private activatedRouter: Router) { }

  ngOnInit() {
    this.client = this.cacheService.get<Client>('userData');

    this.fetchDetails();
  }

  fetchDetails() {
    this.clientService.fetchClientDetails(this.client)
      .subscribe(clientDetail => {
        this.clientDetails = clientDetail;
        this.cacheService.set('clientDetails', clientDetail);
    });
  }

  addAccount() {
    this.activatedRouter.navigate(['/new']);
  }
}
