export class Client {
  public email: string;
  public id: string;
  public expiresIn: number;
  public token: string;

  constructor(email: string, id: string, expiresIn: number, token: string) {
    this.email = email;
    this.id = id;
    this.expiresIn = expiresIn;
    this.token = token;
  }
}
