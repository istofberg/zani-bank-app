import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { ClientComponent } from './client/client.component';
import { AuthGuard } from './auth/auth.guard';
import { NewAccountComponent } from './account/new-account/new-account.component';

const routes: Routes = [
  { path: '', redirectTo: '/client' , pathMatch: 'full'},
  { path: 'login', component: LoginComponent, canActivate: [AuthGuard] },
  { path: 'client', component: ClientComponent, canActivate: [AuthGuard]},
  { path: 'new', component: NewAccountComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

