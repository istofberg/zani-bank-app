import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Account } from './account';
import { ClientService } from '../client/client.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  constructor(private http: HttpClient,
              private clientService: ClientService) {}

  fetchAccountDetails(accountNumber: number, token: string): Observable<Account> {
    const url = environment.fetchAccountDetailsUrl.replace('${accountNumber}', accountNumber.toString())
                                                  .replace('${token}', token);
    return this.http.get<{balance: number, overdraft: number}>(url).pipe(
      map(response => {
        return new Account(accountNumber, response.balance, response.overdraft);
      })
    );
  }

  updateAccount(account: Account, token: string) {
    const url = environment.updateAccountUrl.replace('${account.accNumber}', account.accNumber.toString());
    this.http.put(url, {
      balance: account.balance,
      overdraft: account.overdraft}).
      subscribe(response => {
        console.log('updateAccount, response: ', response);
      });
  }

  addAccount(account: Account, token: string): Observable<Account> {
    const url = environment.addAccountUrl.replace('${account.accNumber}', account.accNumber.toString())
                                         .replace('${token}', token).replace('${token}', token);
    return this.http.put(url, {
      balance: account.balance,
      overdraft: account.overdraft
    }).pipe(map(result => {
      return account;
    }));
  }

  generateAccountNumber(): Account {
    const newNumber = Math.floor((Math.random() * 10000000000) + 1);
    return new Account(newNumber, 0, 0);
  }

}
