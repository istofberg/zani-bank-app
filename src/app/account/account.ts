export class Account {
  public accNumber: number;
  public balance: number;
  public overdraft: number;

  constructor(accNumber: number, balance: number, overdraft: number) {
    this.accNumber = accNumber;
    this.balance = balance;
    this.overdraft = overdraft;
  }

}
