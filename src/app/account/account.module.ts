import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountComponent } from './account.component';
import { AccountListComponent } from './account-list/account-list.component';
import { NewAccountComponent } from './new-account/new-account.component';
import { AccountService } from './account.service';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AccountComponent,
    AccountListComponent,
    NewAccountComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    AccountListComponent
  ],
  providers: [
    AccountService
  ]
})
export class AccountModule { }
