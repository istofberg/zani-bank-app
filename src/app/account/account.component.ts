import { Component, OnInit, Input } from '@angular/core';
import { CacheService } from 'src/app/shared/cache.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Client } from '../client/client';
import { AccountService } from './account.service';
import { Account } from './account';

@Component({
  selector: '[app-account]',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  account: Account;
  @Input() accountNumber: number;
  @Input() index: number;
  client: Client;
  closeResult: string;
  transactionsForm: FormGroup;

  constructor(private accountService: AccountService,
              private cacheService: CacheService,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.fetchAccountDetails();
    // Validation is done in this.fetchAccountDetails()
    this.transactionsForm = new FormGroup({
        amount: new FormControl(null, [Validators.required]),
        transactionType: new FormControl(null, [Validators.required])
      });
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      if (result === 'ok') {
        if (this.transactionsForm.value.transactionType === 'deposit') {
          this.deposit(+this.transactionsForm.value.amount);
        } else if (this.transactionsForm.value.transactionType === 'withdraw') {
          this.withdraw(+this.transactionsForm.value.amount);
        }
        if (this.okAllowed) {
          this.updateAccount();
        }
      }
      this.transactionsForm.reset();
    }, (reason) => {
      console.log(`Dismissed! ${reason}`);
    });
  }

  fetchAccountDetails() {
    this.client = this.cacheService.get<Client>('userData');
    if (this.client) {
      this.accountService.fetchAccountDetails(this.accountNumber, this.client.token).
        subscribe(response => {
          this.account = new Account(response.accNumber, response.balance, response.overdraft);
          // this.transactionsForm.get('amount').setValidators([Validators.required,
          //                                         Validators.pattern(/^[1-9]+[0-9]*$/)]);
        });
    }
  }

  typeChanged() {
    if (this.transactionsForm.value.transactionType === 'withdraw') {
      this.transactionsForm.get('amount').setValidators([Validators.required,
        Validators.max(this.account.balance + this.account.overdraft),
        Validators.pattern(/^[1-9]+[0-9]*$/)]);
    } else {
      this.transactionsForm.get('amount').setValidators([Validators.required,
        Validators.pattern(/^[1-9]+[0-9]*$/)]);
      }
    this.transactionsForm.setValue(this.transactionsForm.value);
  }

  okAllowed() {
    return this.transactionsForm.valid;
  }

  updateAccount() {
    this.accountService.updateAccount(this.account, this.client.token);
  }

  deposit(amount: number) {
    this.account.balance += amount;
  }

  withdraw(amount: number) {
    this.account.balance -= amount;
  }


}
