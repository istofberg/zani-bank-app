import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Account } from '../account';
import { AccountService } from '../account.service';
import { CacheService } from 'src/app/shared/cache.service';
import { Router } from '@angular/router';
import { ClientDetails } from 'src/app/client/client-details';
import { ClientService } from 'src/app/client/client.service';
import { Client } from 'src/app/client/client';

@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.css']
})
export class NewAccountComponent implements OnInit {

  newAccountForm: FormGroup;
  newAccount: Account;
  clientDetails: ClientDetails;
  client: Client;

  constructor(private accountService: AccountService,
              private clientService: ClientService,
              private cacheService: CacheService,
              private activatedRouter: Router) {}

  ngOnInit() {
    this.newAccount = this.accountService.generateAccountNumber();
    this.client = this.cacheService.get('userData');
    this.clientDetails = this.cacheService.get('clientDetails');

    this.newAccountForm = new FormGroup({
      balance: new FormControl(null, [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)]),
      overdraft: new FormControl(null, [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)]),
    });
  }

  addAccount() {
    this.setAmountValues();
    this.accountService.addAccount(this.newAccount, this.client.token)
    .subscribe(newAccount => {
      const newAccountList = this.clientDetails.accounts.slice();
      newAccountList.push(newAccount.accNumber);

      this.clientService.addAccount(this.client, newAccountList).subscribe(result => {
        this.clientDetails.accounts = newAccountList;
        this.cacheService.set('clientDetails', this.clientDetails);
        this.activatedRouter.navigate(['/client']);
      });
    });
  }

  setAmountValues() {
    this.newAccount.balance = +(this.newAccountForm.value.balance);
    this.newAccount.overdraft = +(this.newAccountForm.value.overdraft);
  }

  cancelAdd() {
    this.activatedRouter.navigate(['/client']);
  }

}
