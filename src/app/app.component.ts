import { Component } from '@angular/core';
import { CacheService } from './shared/cache.service';
import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private cacheService: CacheService,
              private authService: AuthService) {}

  logout() {
    this.authService.logoutUser();
  }

  displayLogoutButton() {
    const userData = this.cacheService.get('userData');
    if (userData != null && userData.token) {
      return true;
    } else {
      return false;
    }
  }
}
